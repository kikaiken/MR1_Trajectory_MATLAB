% 経路の数を指定
num_of_path = 12;

% 経由点を設定
% 草原エリア通過

via_point_start2gerge = [
    -0.50 0.50;
    
    -1.72 1.60;
    -1.72 2.30;
    
    -1.225 2.75;
    
    -0.74 3.20;
    -0.74 3.80;
    
    -1.225 4.25;
    
    -1.73 4.70;
    -1.73 5.30;
    -1.35 5.90;
    
    
    %橋入り口・出口
    -1.22 6.50;
    -1.26 8.25;
    
    %橋〜受け渡し
    -2.10 8.65;
    -3.00 8.50;
    -4.30 8.50;
    -4.90 8.30;
    -5.05 7.80;
    %-5.20 7.30;
];

angle_start2gerge = [
    0;
    0;
    1;
];

% シャガイ1個目ピックアップポジションへ向かう
via_point_shagai1_pickup_go = [
    -5.00 8.37;
    -7.00 8.37;
%{
    -6.10 8.31;
    -4.50 8.80;
%}
];
angle_shagai1_pickup_go = [
    0;
    0;
    1;
];

% シャガイ1個目ピックアップポジションから帰る
via_point_shagai1_pickup_back = [
    -3.90 8.50;
    -3.90 4.55;

];
angle_shagai1_pickup_back = [
    0;
    0;
    1;
];

% シャガイ投擲
via_point_shagai_throw = [
    -3.50 8.50;
    -3.55 6.60;
    -3.93 4.65;
];
angle_shagai_throw = [
    0;
    pi * 8 / 9;
    1;
];

% シャガイ2個目ピックアップポジションへ向かう
via_point_shagai2_pickup_go = [
    -3.90 4.55;
    -3.55 6.60;
    -3.50 8.80;
];
angle_shagai2_pickup_go = [
    0;
    0;
    1;
];

% シャガイ2個目ピックアップポジションから投擲位置へ
via_point_shagai2_pickup_throw = [
    -3.50 8.80;
    -3.55 6.60;
    -3.90 4.55;
];
angle_shagai2_pickup_throw = [
    0;
    0;
    1;
];

% シャガイ3個目ピックアップポジションへ向かう
via_point_shagai3_pickup_go = [
    -3.90 4.55;
    -3.50 6.60;
    -3.30 8.10;
    -2.50 8.80;
];
angle_shagai3_pickup_go = [
    0;
    0;
    1;
];

% シャガイ3個目ピックアップポジションから投擲位置へ
via_point_shagai3_pickup_throw = [
    -2.50 8.80;
    -3.30 8.10;
    -3.50 6.60;
    -3.90 4.55;
];
angle_shagai3_pickup_throw = [
    0;
    0;
    1;
];

via_point_test1 = [
    -0.50 0.50;
    -0.50 1.50;
];
angle_test1 = [
    0;
    0;
    1;
];

via_point_test2 = [
    -0.50 1.50;
    -1.50 1.50;
];   
angle_test2 = [
    0;
    0;
    1;
];

via_point_test3 = [
    -5.150 7.80;
    -5.150 4.30;
];   
angle_test3 = [
    0;
    0;
    1;
];

via_point_test4 = [
    -4.500 3.65;
    -3.200 2.55;
];    
angle_test4 = [
    pi / 4;
    pi / 4;
    1;
];

% 走行パラメータを設定
velocity_max = 5.0;          %最高速
lateral_acc_max = 4.0;      %ターン時の横G
acc_max = 3.0;              %加速度
angvel_max = pi;            %最大角速度
angacc_max = pi;            %角加速度

% 制御周期
CTRL_CYCLE = 0.001;

%機体サイズ
MR1_LENGTH = 0.67;
MR1_WIDTH = 0.6;