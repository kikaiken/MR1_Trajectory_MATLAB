% 曲率マップを作成
section = 0.1;      %曲率を求めるときの区間の幅
rho = zeros(trajectory_length, 1);  %曲率
rho_pm = zeros(trajectory_length, 1);
range = 1 : trajectory_length;
for i = ceil(section * 100 / 2) + 3 : trajectory_length - (floor(section * 100 / 2) + 1)
    fdiff_x = trajectory(i + round(section * 100 / 2) + 1, 1) - trajectory(i + round(section * 100 / 2) - 1, 1);
    fdiff_y = trajectory(i + round(section * 100 / 2) + 1, 2) - trajectory(i + round(section * 100 / 2) - 1, 2);
    if (fdiff_y >= 0)       %第1象限・第2象限
        ftheta = atan2(fdiff_y, fdiff_x);
    elseif (fdiff_y < 0)    %第3象限・第4象限
        ftheta = 2 * pi + atan2(fdiff_y, fdiff_x);
    end

    bdiff_x = trajectory(i - round(section * 100 / 2) + 1, 1) - trajectory(i - round(section * 100 / 2) - 1, 1);
    bdiff_y = trajectory(i - round(section * 100 / 2) + 1, 2) - trajectory(i - round(section * 100 / 2) - 1, 2);
    if (bdiff_y >= 0)
        btheta = atan2(bdiff_y, bdiff_x);
    elseif (bdiff_y < 0)
        btheta = 2 * pi + atan2(bdiff_y, bdiff_x);
    end
    
    % 曲率の絶対値を求める
    theta_diff = abs(ftheta - btheta);
    if (theta_diff > pi)
        theta_diff = 2 * pi - theta_diff;
    end
    rho(i, 1) = theta_diff / section;
end

figure(2);
subplot(4, 1, 1);
plot(range, rho(:, 1));
title('曲率の絶対値');
xlabel('距離(cm)');
ylabel('曲率');

% 速度制限設定
spd = zeros(trajectory_length, 1);  %速度
for i = 1 : trajectory_length
    if (rho(i, 1) < 0.3)
        spd(i) = vel_max;
    else
        spd(i) = sqrt(lateral_acc / rho(i, 1));
        if (spd(i) > vel_max)
            spd(i) = vel_max;
        end
    end
end

subplot(4, 1, 2);
plot(range, spd(:, 1));
title('横Gによる制限速度');
xlabel('距離(cm)');
ylabel('速さ(m/s)');

% 台形加減速による速度設定
spd(1) = 0;
spd(trajectory_length) = 0;
for i = 1 : (trajectory_length-1)
    spd(i+1, 1) = min(spd(i+1), sqrt((spd(i)).^2 + 2 * acc_max * 0.01));
end
for i = (trajectory_length-1) : -1 : 1
    spd(i) = min(spd(i), sqrt((spd(i+1)).^2 + 2 * acc_max * 0.01));
end
subplot(4, 1, 3);
plot(range, spd(:));
title('加減速');
xlabel('距離(cm)');
ylabel('速さ(m/s)');

% 目標角度設定
if trajectory_num == 1
    angvel = ones(max(abs(round((angle_start2gerge(2) - angle_start2gerge(1)) * 1000)), 2), 1) * angvel_max;
    angvel(1) = 0;
    angvel(size(angvel)) = 0;
    angle_start = angle_start2gerge(1);
elseif trajectory_num == 2
    angvel = ones(max(abs(round((angle_shagai_position(2) - angle_shagai_position(1)) * 1000)), 2), 1) * angvel_max;
    angvel(1) = 0;
    angvel(size(angvel)) = 0;
    angle_start = angle_shagai_position(1);
elseif trajectory_num == 3
    angvel = ones(max(abs(round((angle_shagai_pickup_1(2) - angle_shagai_pickup_1(1)) * 1000)), 2), 1) * angvel_max;
    angvel(1) = 0;
    angvel(size(angvel)) = 0;
    angle_start = angle_shagai_pickup_1(1);
elseif trajectory_num == 4
    angvel = ones(max(abs(round((angle_shagai_throw_1(2) - angle_shagai_throw_1(1)) * 1000)), 2), 1) * angvel_max;
    angvel(1) = 0;
    angvel(size(angvel)) = 0;
    angle_start = angle_shagai_throw_1(1);
end

for i = 1 : size(angvel) - 1
    angvel(i+1) = min(angvel(i+1), sqrt((angvel(i)).^2 + 2 * angacc_max * 0.001));
end
for i = size(angvel) - 1 : -1 : 1
    angvel(i) = min(angvel(i), sqrt((angvel(i+1)).^2 + 2 * angacc_max * 0.001));
end
angvel
subplot(4, 1, 4);
plot(angvel);
title('Yaw角度');
xlabel('角度(rad)');
ylabel('目標角度(rad)');