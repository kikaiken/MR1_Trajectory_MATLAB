% 変数消去
clear;

%Clothoidをインポート
addpath('./Clothoids/matlab')

% パラメータ読み込み
Parameter

% フィールド描画
DrawField(1);

for path_num = 1 : num_of_path
    if path_num == 1
        via_point = via_point_start2gerge;
        angle_start_end = angle_start2gerge;
    elseif path_num == 2
        via_point = via_point_shagai1_pickup_go;
        angle_start_end = angle_shagai1_pickup_go;
    elseif path_num == 3
        via_point = via_point_shagai1_pickup_back;
        angle_start_end = angle_shagai1_pickup_back;
    elseif path_num == 4
        via_point = via_point_shagai_throw;
        angle_start_end = angle_shagai_throw;
    elseif path_num == 5
        via_point = via_point_shagai2_pickup_go;
        angle_start_end = angle_shagai2_pickup_go;
    elseif path_num == 6
        via_point = via_point_shagai2_pickup_throw;
        angle_start_end = angle_shagai2_pickup_throw;
    elseif path_num == 7
        via_point = via_point_shagai3_pickup_go;
        angle_start_end = angle_shagai3_pickup_go;
    elseif path_num == 8
        via_point = via_point_shagai3_pickup_throw;
        angle_start_end = angle_shagai3_pickup_throw;
    elseif path_num == 9
        via_point = via_point_test1;
        angle_start_end = angle_test1;
    elseif path_num == 10
        via_point = via_point_test2;
        angle_start_end = angle_test2;
    elseif path_num == 11
        via_point = via_point_test3;
        angle_start_end = angle_test3;
    elseif path_num == 12
        via_point = via_point_test4;
        angle_start_end = angle_test4;
    end
    
    % スプラインを計算
    [path, path_length] = MakePathClothoid(via_point);
    
    % 曲率マップを生成
    [curvature_map] = MakeCurvatureMap(path, path_length);
    
    % 速度マップを生成
    [velocity_map] = MakeVelocityMap(path_length, curvature_map, velocity_max, lateral_acc_max, acc_max);
    
    % 角速度マップを生成
    [angvel_map] = MakeAngVelMap(angle_start_end, angvel_max, angacc_max);
    
    % 経由点表示
    figure(1);
    scatter(via_point(:, 1), via_point(:, 2), 'filled');
    plot(path(:, 1), path(:, 2));
    
    figure(2);
    subplot(num_of_path, 1, path_num);
    plot(curvature_map(:));
    title('曲率の絶対値');
    xlabel('距離(cm)');
    ylabel('曲率');
    
    figure(3);
    subplot(num_of_path, 1, path_num);
    plot(velocity_map(:));
    title('速度マップ');
    xlabel('距離(cm)');
    ylabel('速さ(m/s)');
    
    figure(4);
    subplot(num_of_path, 1, path_num);
    plot(angvel_map(:));
    title('角速度マップ');
    xlabel('角度(rad)');
    ylabel('角速度(rad/s)');
    
    % 経路追従シミュレーション
    [actual_path, angle_ref, expected_time] = PurePursuit(path, path_length, velocity_map, angvel_map, angle_start_end(1), CTRL_CYCLE);
    % Pure Pursuit走行経路表示
    DrawField(5);
    plot(actual_path(:, 1), actual_path(:, 2), 'r');
    % Gif保存
    MakeGif(path_num, actual_path, expected_time, MR1_LENGTH, MR1_WIDTH);
    
    % キーボード入力待ち
    waitforbuttonpress;
end


%----------------------------------------------------------
%   関数
%----------------------------------------------------------
% フィールド図を描く
function [] = DrawField(fig_num)
    figure(fig_num);
    img = imread('NHK2019_Field.png');  %画像読み込み
    image([0, -13.3], [0, 10], img);    %表示
    pbaspect([13.3, 10, 1]);            %縦横比を実際と合わせる
    % 軸反転
    ax = gca;
    ax.XDir = 'reverse';
    hold on;                            %重ね合わせOK
end

% スプラインを計算し、さらに点群を1cmごとの点になるように間引きする
function [path, path_length] = MakePath(via)
    % スプラインを計算
    [n_via, ~] = size(via);
    t = 1 : n_via;
    ts = 1 : 1/5000 : n_via;
    xs = spline(t, via(:,1), ts);
    ys = spline(t, via(:,2), ts);
    %figure(1);
    %plot(xs,ys,'r');

    % 点間の距離を計算
    [~, n_spline_point] = size(xs);
    dist = zeros(n_spline_point, 1);
    for i = 1 : n_spline_point - 1
        dist(i, 1) = hypot(xs(i+1) - xs(i), ys(i+1) - ys(i));
    end

    % 1cmごとの点を軌道として保存
    path_length_max = 2000;
    path = zeros(path_length_max, 2);
    path(1, 1) = via(1, 1);     %x
    path(1, 2) = via(1, 2);     %y
    dist_buf = 0;
    k = 1;
    for i = 1 : n_spline_point
        dist_buf = dist_buf + dist(i, 1);
        if dist_buf > 0.0099
            k = k + 1;
            path(k, 1) = xs(i);
            path(k, 2) = ys(i);
            dist_buf = 0;
        end
    end

    path_length = k;
    % trajectoryの無駄な要素を削除
    for i = path_length_max : -1 : path_length + 1
        path(i, :) = [];
    end
end

% クロソイド曲線版MakeP
function [path, path_length] = MakePathClothoid(via)
    % クロソイドを計算
    S = ClothoidSplineG2();
    SL = S.buildP2( via(:,1), via(:,2));
    point=SL.out(5000);
    xs=point(1,:);
    ys=point(2,:);

    %figure(1);
    %plot(xs,ys,'r');

    % 点間の距離を計算
    [~, n_spline_point] = size(xs);
    dist = zeros(n_spline_point, 1);
    for i = 1 : n_spline_point - 1
        dist(i, 1) = hypot(xs(i+1) - xs(i), ys(i+1) - ys(i));
    end

    % 1cmごとの点を軌道として保存
    path_length_max = 2000;
    path = zeros(path_length_max, 2);
    path(1, 1) = via(1, 1);     %x
    path(1, 2) = via(1, 2);     %y
    dist_buf = 0;
    k = 1;
    for i = 1 : n_spline_point
        dist_buf = dist_buf + dist(i, 1);
        if dist_buf > 0.0099
            k = k + 1;
            path(k, 1) = xs(i);
            path(k, 2) = ys(i);
            dist_buf = 0;
        end
    end

    path_length = k;
    % trajectoryの無駄な要素を削除
    for i = path_length_max : -1 : path_length + 1
        path(i, :) = [];
    end
end

% 曲率マップを作成
function [curvature_map] = MakeCurvatureMap(path, path_length)
    section = 0.1;      %曲率を求めるときの区間の幅
    curvature_map = zeros(path_length, 1);  %曲率
    for i = ceil(section * 100 / 2) + 3 : path_length - (floor(section * 100 / 2) + 1)
        fdiff_x = path(i + round(section * 100 / 2) + 1, 1) - path(i + round(section * 100 / 2) - 1, 1);
        fdiff_y = path(i + round(section * 100 / 2) + 1, 2) - path(i + round(section * 100 / 2) - 1, 2);
        if (fdiff_y >= 0)       %第1象限・第2象限
            ftheta = atan2(fdiff_y, fdiff_x);
        elseif (fdiff_y < 0)    %第3象限・第4象限
            ftheta = 2 * pi + atan2(fdiff_y, fdiff_x);
        end
        
        bdiff_x = path(i - round(section * 100 / 2) + 1, 1) - path(i - round(section * 100 / 2) - 1, 1);
        bdiff_y = path(i - round(section * 100 / 2) + 1, 2) - path(i - round(section * 100 / 2) - 1, 2);
        if (bdiff_y >= 0)
            btheta = atan2(bdiff_y, bdiff_x);
        elseif (bdiff_y < 0)
            btheta = 2 * pi + atan2(bdiff_y, bdiff_x);
        end
        
        % 曲率の絶対値を求める
        theta_diff = abs(ftheta - btheta);
        if (theta_diff > pi)
            theta_diff = 2 * pi - theta_diff;
        end
        curvature_map(i, 1) = theta_diff / section;
    end
end

% 速度マップ生成
function [velocity_map] = MakeVelocityMap(path_length, curvature_map, vel_max, lat_acc_max, acc_max)
    % 制限速度設定
    velocity_map = zeros(path_length, 1);  %速度
    for i = 1 : path_length
        if (curvature_map(i, 1) < 0.3)
            velocity_map(i) = vel_max;
        else
            velocity_map(i) = sqrt(lat_acc_max / curvature_map(i, 1));
            if (velocity_map(i) > vel_max)
                velocity_map(i) = vel_max;
            end
        end
    end

    % 速度設定
    velocity_map(1) = 0;
    velocity_map(path_length) = 0;
    for i = 1 : (path_length - 1)
        velocity_map(i + 1, 1) = min(velocity_map(i + 1), sqrt((velocity_map(i)).^2 + 2 * acc_max * 0.01));
    end
    for i = (path_length - 1) : -1 : 1
        velocity_map(i) = min(velocity_map(i), sqrt((velocity_map(i + 1)).^2 + 2 * acc_max * 0.01));
    end
end

% 角速度マップ作成
function [angvel_map] = MakeAngVelMap(angle_start_end, angvel_max, angacc_max)
    angvel_map = ones(max(abs(round((angle_start_end(3) * angle_start_end(2) - angle_start_end(1)) * 1000)), 2), 1) * angvel_max;
    angvel_map(1) = 0;
    angvel_map(size(angvel_map)) = 0;
    for i = 1 : size(angvel_map) - 1
        angvel_map(i+1) = min(angvel_map(i+1), sqrt((angvel_map(i)).^2 + 2 * angacc_max * 0.001));
    end
    for i = size(angvel_map) - 1 : -1 : 1
        angvel_map(i) = min(angvel_map(i), sqrt((angvel_map(i+1)).^2 + 2 * angacc_max * 0.001));
    end
    angvel_map = angvel_map * angle_start_end(3);
end

% Pure Pursuit
function [actual_path, angle_ref, expected_time_ms] = PurePursuit(path, path_length, velocity_map, angvel_map, start_angle, CTRL_CYCLE)
    pos = [path(1, 1) path(1, 2) start_angle];  %現在位置
    actual_path = [path(1, 1), path(1, 2), start_angle];
    angle_ref = [start_angle; start_angle;];
    time_ms = 1;
    nearest_index = 1;
    while nearest_index < path_length
        prev_nearest_index = nearest_index;
        % 前回のnearest_indexの前後10cmの中から、新たに最も近い点を探す
        min_length = 1;
        %[path_length, nearest_index]
        for i = max(nearest_index - 10, 1) : min(nearest_index + 10, path_length)
            if (hypot((pos(1) - path(i, 1)).^2, (pos(2) - path(i, 2)).^2) < min_length)
                nearest_index = max(i, 2);
                min_length = hypot((pos(1) - path(i, 1)).^2, (pos(2) - path(i, 2)).^2);
            end
        end
        % 目標点を決める
        reference_index = min(nearest_index + round(velocity_map(nearest_index) * 10) + 1, path_length);
        
        % 現在位置から目標点に向かう向きを求める
        diff_x = path(reference_index, 1) - pos(1);
        diff_y = path(reference_index, 2) - pos(2);
        
        % 現在位置更新
        pos(1) = pos(1) + velocity_map(nearest_index) * diff_x / hypot(diff_x, diff_y) * CTRL_CYCLE;
        pos(2) = pos(2) + velocity_map(nearest_index) * diff_y / hypot(diff_x, diff_y) * CTRL_CYCLE;
        pos(3) = pos(3) + angvel_map(round(abs((pos(3) - start_angle)) * 1000) + 2) * CTRL_CYCLE;
        
        % 走行距離に対する角度を保存
        if nearest_index > (prev_nearest_index + 1)
            for i = prev_nearest_index : nearest_index
                angle_ref(i) = pos(3);
            end
        else
            angle_ref(nearest_index) = pos(3);
        end
        
        % Pure Pursuit走行経路保存
        actual_path(time_ms, 1) = pos(1);
        actual_path(time_ms, 2) = pos(2);
        actual_path(time_ms, 3) = pos(3);
        time_ms = time_ms + 1;
    end
    
    % 予想タイム表示
    [expected_time_ms, ~] = size(actual_path);
    expected_time_s = expected_time_ms * CTRL_CYCLE;
    fprintf("予想タイム = %f秒\n", expected_time_s);
end

% 超信地旋回
function [spin_path] = SpinTurn(pos, angvel_map)
    
end

% 機体の通過経路を表示し、実時間でGifファイルに保存
function [] = MakeGif(path_num, actual_path, expected_time_ms, MR1_LENGTH, MR1_WIDTH)
    % MR1通過経路表示
    fig = figure(5);
    for i = 1 : 100 : expected_time_ms
        x = [actual_path(i,1)-(MR1_WIDTH/2) actual_path(i,1)+(MR1_WIDTH/2) actual_path(i,1)+(MR1_WIDTH/2) actual_path(i,1)-(MR1_WIDTH/2)];
        y = [actual_path(i,2)-(MR1_LENGTH/2) actual_path(i,2)-(MR1_LENGTH/2) actual_path(i,2)+(MR1_LENGTH/2) actual_path(i,2)+(MR1_LENGTH/2)];
        mr1_rectangle = polyshape(x, y);
        mr1_rotate = rotate(mr1_rectangle, actual_path(i, 3) * 180 / pi, [actual_path(i,1) actual_path(i,2)]);
        plot(mr1_rotate, 'FaceColor', 'black', 'FaceAlpha', 0.01)
        drawnow;
        frame = getframe(fig);
        im{i} = frame2im(frame);
    end

    % gif保存
    filename = sprintf('Animation%d.gif', path_num);
    for i = 1 : 100 : expected_time_ms
        [A,map] = rgb2ind(im{i},256);
        if i == 1
            imwrite(A,map,filename,'gif','LoopCount',Inf,'DelayTime',0.1);
        else
            imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',0.1);
        end
    end
end